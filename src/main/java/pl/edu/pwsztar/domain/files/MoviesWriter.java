package pl.edu.pwsztar.domain.files;

import java.io.BufferedWriter;
import java.io.FileOutputStream;

public interface MoviesWriter {
    BufferedWriter writeFilmsToFile(FileOutputStream fos);
}
