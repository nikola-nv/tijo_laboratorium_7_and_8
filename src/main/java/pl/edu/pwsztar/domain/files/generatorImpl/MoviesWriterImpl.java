package pl.edu.pwsztar.domain.files.generatorImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.comparator.SortMoviesByYear;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.files.MoviesWriter;
import pl.edu.pwsztar.service.MovieService;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.List;

@Service
public class MoviesWriterImpl implements MoviesWriter {
    private MovieService movieService;

    @Autowired
    public MoviesWriterImpl(MovieService movieRepository){
        this.movieService = movieRepository;
    }

    @Override
    public BufferedWriter writeFilmsToFile(FileOutputStream fos) {
        BufferedWriter bw = null;
        List<MovieDto> movies = movieService.findAll();

        movies.sort(new SortMoviesByYear());
        Collections.reverse(movies);

        try {
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            for (MovieDto movie : movies) {
                bw.write(movie.getYear() + " " + movie.getTitle());
                bw.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return bw;
    }
}
