package pl.edu.pwsztar.domain.files;

import org.springframework.core.io.InputStreamResource;
import pl.edu.pwsztar.domain.dto.FileDto;

import java.io.File;


public interface FileGenerator{
    InputStreamResource generateFile(FileDto fileDto);
    Long getFileLength();
}
