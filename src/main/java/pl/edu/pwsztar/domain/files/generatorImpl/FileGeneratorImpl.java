package pl.edu.pwsztar.domain.files.generatorImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FileDto;

import pl.edu.pwsztar.domain.files.FileGenerator;

import java.io.*;

@Service
public class FileGeneratorImpl implements FileGenerator {
    private Long fileLength;
    private MoviesWriterImpl moviesWriter;


    @Autowired
    public FileGeneratorImpl(MoviesWriterImpl moviesWriter) {
        this.moviesWriter = moviesWriter;
    }


    @Override
    public InputStreamResource generateFile(FileDto fileDto) {
        try {
            File file = File.createTempFile(fileDto.getName(), fileDto.getFormat());
            FileOutputStream fos = new FileOutputStream(file);

            BufferedWriter bw = moviesWriter.writeFilmsToFile(fos);

            bw.close();

            fos.flush();
            fos.close();

            this.fileLength = file.length();
            InputStream stream = new FileInputStream(file);

            return new InputStreamResource(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long getFileLength() {
        return fileLength;
    }
}
