package pl.edu.pwsztar.domain.files.generatorImpl;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.files.FileResponse;

import java.util.Date;

@Service
public class FileResponseImpl implements FileResponse {

    @Override
    public ResponseEntity<Resource> returnState(FileDto file ,Long length , InputStreamResource inputStreamResource) {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName() +"_"+ (new Date().getTime())+ file.getFormat())
                .contentLength(length)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(inputStreamResource);
    }
}
