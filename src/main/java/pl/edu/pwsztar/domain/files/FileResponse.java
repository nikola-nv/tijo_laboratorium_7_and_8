package pl.edu.pwsztar.domain.files;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import pl.edu.pwsztar.domain.dto.FileDto;

public interface FileResponse {
    ResponseEntity<Resource> returnState(FileDto file , Long length , InputStreamResource inputStreamResource);
}
