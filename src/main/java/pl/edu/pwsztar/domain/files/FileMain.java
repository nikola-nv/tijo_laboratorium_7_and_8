package pl.edu.pwsztar.domain.files;


import pl.edu.pwsztar.domain.dto.FileReturnDto;

public interface FileMain {
    FileReturnDto generateFile();
}
