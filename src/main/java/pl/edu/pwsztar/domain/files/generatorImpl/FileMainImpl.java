package pl.edu.pwsztar.domain.files.generatorImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.FileReturnDto;
import pl.edu.pwsztar.domain.files.FileGenerator;
import pl.edu.pwsztar.domain.files.FileMain;

@Service
public class FileMainImpl implements FileMain {
    private FileGenerator fileGenerator;

    @Autowired
    public FileMainImpl(FileGenerator fileGenerator) {
        this.fileGenerator = fileGenerator;
    }

    @Override
    public FileReturnDto generateFile() {
        String fileName = "movies";
        String fileFormat = ".txt";
        Long fileLength = null;
        FileDto file = null;

        file = new FileDto.Builder().name(fileName).format(fileFormat).build();

        InputStreamResource fileResource = fileGenerator.generateFile(file);

        fileLength = fileGenerator.getFileLength();

        return new FileReturnDto.Builder().resource(fileResource).file(file).length(fileLength).build();
    }

}
