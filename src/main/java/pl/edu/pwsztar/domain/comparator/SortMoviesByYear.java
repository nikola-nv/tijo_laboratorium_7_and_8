package pl.edu.pwsztar.domain.comparator;

import pl.edu.pwsztar.domain.dto.MovieDto;

import java.util.Comparator;

public class SortMoviesByYear implements Comparator<MovieDto> {
    @Override
    public int compare(MovieDto firstMovie, MovieDto secondMovie) {
        return firstMovie.getYear().compareTo(secondMovie.getYear());
    }
}
