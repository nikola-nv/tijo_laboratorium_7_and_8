package pl.edu.pwsztar.domain.dto;

import java.io.Serializable;

public class FileDto implements Serializable {
    private String name;
    private String format;

    public FileDto(Builder builder) {
        this.name = builder.name;
        this.format = builder.format;
    }

    public String getName() {
        return name;
    }

    public String getFormat() {
        return format;
    }

    public static final class Builder{
        private String name;
        private String format;

        public Builder() {
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Builder format(String format){
            this.format = format;
            return this;
        }

        public FileDto build(){
            return new FileDto(this);
        }
    }

}
