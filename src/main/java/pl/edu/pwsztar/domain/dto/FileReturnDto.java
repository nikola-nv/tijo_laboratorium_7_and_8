package pl.edu.pwsztar.domain.dto;

import org.springframework.core.io.InputStreamResource;

import java.io.Serializable;

public class FileReturnDto implements Serializable {
    private InputStreamResource resource;
    private FileDto file;
    private Long length;

    public FileReturnDto(Builder builder) {
        this.resource = builder.resource;
        this.file = builder.file;
        this.length = builder.length;
    }

    public InputStreamResource getResource() {
        return resource;
    }

    public FileDto getFile() {
        return file;
    }

    public Long getLength() {
        return length;
    }

    public static final class Builder{
        private InputStreamResource resource;
        private FileDto file;
        private Long length;

        public Builder resource(InputStreamResource resource){
            this.resource = resource;
            return this;
        }

        public Builder file(FileDto file){
            this.file = file;
            return this;
        }

        public Builder length(Long length){
            this.length = length;
            return this;
        }

        public FileReturnDto build(){
            return new FileReturnDto(this);
        }
    }
}
