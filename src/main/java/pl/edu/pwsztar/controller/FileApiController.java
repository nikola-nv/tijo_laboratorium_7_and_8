package pl.edu.pwsztar.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pwsztar.domain.dto.FileReturnDto;
import pl.edu.pwsztar.domain.files.FileMain;
import pl.edu.pwsztar.domain.files.FileResponse;


@Controller
@RequestMapping(value="/api")
public class FileApiController {
    private FileMain fileMain;
    private FileResponse fileResponse;

    @Autowired
    public FileApiController(FileMain fileMain , FileResponse fileResponse){
        this.fileMain = fileMain;
        this.fileResponse = fileResponse;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @CrossOrigin
    @GetMapping(value = "/download-txt")
    public ResponseEntity<Resource> downloadTxt(){
        LOGGER.info("--- download txt file ---");

        FileReturnDto file = fileMain.generateFile();
        return fileResponse.returnState(file.getFile() , file.getLength() , file.getResource());
    }
}
